# <font color="Darkblue"> Challenge Coordinadora Mercantil </font>

<font color="Darkblue">*Prueba Tecnica*</font>
 
## <font color="Darkblue">Descripción</font>
Esta API realiza todas las operaciones necesarias para superar el reto de la compañia Coordinadora, a continuación se muestran las respuestas a las preguntas planteadas y los links que fueron solicitados (youtube y documentación en swagger).

## <font color="Darkblue"> Preguntas </font>
**1.** **Con sus propias palabras explique que es una inyección de dependencias y para que sirve**
Es un patrón de diseño que sirve para resolver el principio de inversión de dependencias, es decir, el objetivo de la inyección de dependencias es eliminar para un constructor o metodo la responsabilidad de creación interna de un objeto de otra clase, la idea es que que la clase reciba el objeto ya hecho y no que tenga este objeto que ser realizado, un ejemplo sencillo de esto, seria el mesero que te sirve la comida en un restaurante, el mesero no tiene porque saber como crear la comida y tampoco tiene la responsabilidad de hacerlo, su unica función es servir la comida.  

**2.** **Explique con sus propias palabras la diferencia entre asincrono y sincrono**
JavaScript solo ejecuta una instrucción a la vez, el codigo sincrono sigue el orden en el que las instrucciones han sido declaradas, por otro lado, el codigo asincrono no sigue el orden en el que las instrucciones se declaran, la asincronia la podemos ver como una lista de tareas que un jefe debe hacer pero que por cuestiones de tiempo no puede y que por esta razón decide delegarlas o dejarlas como tareas pendientes para despues retomarlas cuando este disponible para resolverlas, es decir, en la asincronia las tareas que necesitamos hacer no se quedan bloqueadas esperando a ser finalizadas, simplemente se mueven a una sala de espera.

**3.** **Cual es la importancia del uso de promesas en un proyecto** 
Las promesas son importantes porque permiten gestionar y admnistrar la asincrona en un proyecto, esto debido a que poseen la capacidad de permanecer en el estado de "pendiente" o "incierto", las promesas garantizan que algo esta por resolverse nosotros sabremos si se tuvo exito o no.

**4.** **Cual es la importancia de usar ORM en un proyecto, ventajas y desventajas**
Un ORM  es importante en un proyecto porque permite mapear de forma automatica las estructuras de una base de datos, entre sus ventajas y desventajas se encuentran las siguientes: 

**Ventajas:**
1. Elimina la necesidad de codigo SQL repetitivo
2. Reduce el tiempo de desarrollo
3. Reduce los costos de desarrollo

**Desventajas:**
1. Lentitud de las peticiones cuando el volumen de datos es elevado
2. El desarrollador posee un poco menos de control de lo que esta haciendo el codigo, se tiene mas control usando SQL puro
 
**5.** **Que diferencia hay entre una base de datos SQL y NoSQL**
Las bases de datos SQL son del tipo relacional, mientras que las NoSQL son no relacionales, una base de datos relacional posee una estructura de datos rigida o determinada que se encuentra expresada en tablas y se que se relacionan a traves de identificadores, mientras que las bases de datos no relacionales poseen una estructura dinamica, esto permite a los objetos de la misma, poseer propiedades diferentes aun cuando son los mismos objetos. 

**6.** **Si hablo de colección y documentos me refiero a**
Las colecciones son simplemente los arrays en los cuales los documentos son almacenados; y los documentos son los datos en formato JSON, existen subdocumentos llamados, documentos embebidos y Nested Paths.

**7.** **Si una aplicación esta sacando error de CORS a que se está refiriendo**
Este error ocurre cuando el servidor no tiene configurado cors o cuando la api no permite que cierto origen acceda a ella, esto quiere decirs que cors funciona como una capa de seguridad extra que restringe el acceso a la api, en node js lo que se hace es instalar con npm cors y configurar los origenes que estan permitidos.

## <font color="Darkblue"> Links </font>
**Swagger**
Para mas información con respecto a los modelos, esquemas y endpoints, se recomienda revisar la documentación de swagger de esta Api: [Swagger Documentation Challenge Coordinadora](http://localhost:3000/api-docs/). 

**Youtube**
El link de youtube en el cual se resuelven dos de los challenges es el siguiente [Link Challenges](https://youtu.be/3BkkmNrXOTw).
