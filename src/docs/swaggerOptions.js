const swaggerOptions = {
    definition: {
      openapi: '3.0.0',
      info: {
        title: 'Challenge - Coordinadora Mercantil',
        description: 'API Challenges',
        version: '1.0.0',
      },
      servers: [
        {
          url: 'http://localhost:3000',
          description: 'Local Server',
        },
      ],
    },
    apis: ['./src/routes/*.js'],
  
  };
  
  export default swaggerOptions;
  