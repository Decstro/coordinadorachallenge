import express, { json } from 'express';
const app = express();
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';
import challengeRoutes from './routes/challenges.route.js';
import swaggerOptions from './docs/swaggerOptions.js';


app.use(json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));


app.use('/challenge', challengeRoutes);

app.listen(3000, () => { console.log('Escuchando desde el puerto 3000...')});