import express, { json } from 'express';
const router = express();
import fetch from 'node-fetch';

router.use(json());

/**
 * @swagger
 * /challenge/1_FindingError/{a}/{b}:
 *   get:
 *       summary: Retorna la operación realizada
 *       tags: [Challenges]
 *       parameters:
 *         - name: a
 *           in: path
 *           required: true
 *           description: Numero entero
 *           schema:
 *               type: integer
 *         - name: b
 *           in: path
 *           required: true
 *           description: Numero entero
 *           schema:
 *               type: integer
 *       responses:
 *           200:
 *               description: Muestra la operación solicitada
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/challenge1'
 */
async function average(a, b) {
  try{
    const response =  await a + b / 2;
    return response;
  }catch(e){
    res.json('A ocurrido un error con este reto');
  }      
};
router.get('/1_FindingError/:a/:b', async (req, res) => {
  const { a, b } = req.params;
  const result = await average(parseInt(a), parseInt(b)); 
  res.json(`El resultado es: ${result}`);
});

/**
 * @swagger
 * /challenge/2_StringManipulation/{word}:
 *   get:
 *       summary: Dada una palabra elimina los signos de exclamación
 *       tags: [Challenges]
 *       parameters:
 *         - name: word
 *           in: path
 *           required: true
 *           description: Palabra a procesar
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Manipula un string y realiza una operación
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/challenge2'
 */
router.get('/2_StringManipulation/:word', (req, res) => {
    const { word } = req.params;
    const wordArray = word.split('');
    let acount = 0; 
    wordArray.forEach( letter => { if(letter === '!') acount += 1 });
    if(acount > 0){
      let aux = 0;
      for (let i = 0; i < wordArray.length; i++) {
        if(wordArray[i] === '!'){
          if(aux === 0) wordArray.splice(i, 1); 
          aux += 1;
        } 
      }
      const result = wordArray.join('');
      res.json(`La palabra: ${word} tenia ${acount} simbolo(s), el resultado es: ${result} `);
    }else res.json(`La palabra: ${word} no posee el signo: ! `);
});

/**
 * @swagger
 * /challenge/3_SumArray/{rowsLength}/{columnsLength}:
 *   get:
 *       summary: Retorna las diferentes operaciones realizadas a la matriz
 *       tags: [Challenges]
 *       parameters:
 *         - name: rowsLength
 *           in: path
 *           required: true
 *           description: Numero de filas de la matriz
 *           schema:
 *               type: integer
 *         - name: columnsLength
 *           in: path
 *           required: true
 *           description: Numero de columnas de la matriz
 *           schema:
 *               type: integer
 *       responses:
 *           200:
 *               description: Muestra las operaciones solicitadas para la matriz
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/challenge3'
 */
function challengeMatriz(matriz, rowsLength, columnsLength){
  if(matriz.length !== 0 && matriz.length !== null && matriz.length !== undefined){
    let resultSum = 0;
    let resultPositive = 0;
    let resultEvenNumbers = 0;
    let resultOddnumbers = 0;
    for (let i = 0; i < rowsLength; i++) {
      for (let j = 0; j < columnsLength; j++) {
        resultSum += matriz[i][j];
        if(matriz[i][j] > 0) resultPositive += matriz[i][j];
        if(matriz[i][j]%2 === 0) resultEvenNumbers += matriz[i][j];
        else  resultOddnumbers += matriz[i][j];
      }
    }   
    const result = {
      sumMatriz: resultSum,
      sumPositiveMatriz: resultPositive,
      sumEvenNumbers: resultEvenNumbers,
      sumOddNumbers: resultOddnumbers,
    }
    return result;
  } else return 0;  
};
router.get('/3_SumArray/:rowsLength/:columnsLength', (req, res) => {
  const { rowsLength, columnsLength } = req.params;
  const matrizNumbers = [...Array(parseInt(rowsLength))].map(e => Array(parseInt(columnsLength)).fill('') );
  for (let i = 0; i < rowsLength; i++) {
    for (let j = 0; j < columnsLength; j++) {
      matrizNumbers[i][j] = Math.floor(Math.random() * (100 - (-100))) -100;   
    }
  }
  console.log('Matriz: ');
  console.log(matrizNumbers);
  const result = challengeMatriz(matrizNumbers, rowsLength, columnsLength);
  res.json(result);
});

/**
 * @swagger
 * /challenge/4_TransformArray/{order}:
 *   get:
 *       summary: Convierte una matriz de dos dimensiones en un array de una dimensión y lo ordena de forma ascendente o descendente
 *       tags: [Challenges]
 *       parameters:
 *         - name: order
 *           in: path
 *           required: true
 *           description: Orden de los numeros en el array
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Manipula un string y realiza una operación
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/challenge4'
 */
function from2Dto1D(matriz, rowsLength, columnsLength, order){
  let oneDimensionArray = [];
  let result;
  for (let i = 0; i < rowsLength; i++) {
    for (let j = 0; j < columnsLength; j++) {
      oneDimensionArray.push(matriz[i][j]);   
    }
  }

  if(order === 'ASC') result = oneDimensionArray.sort((a, b) => a - b );
  else if(order === 'DESC') result = oneDimensionArray.sort((a, b) => b - a );
  else result = 'La variable order debe ser DESC o ASC';
  return result;
}
router.get('/4_TransformArray/:order', (req, res) => {
    const { order } = req.params;
    const rowsLength = 5;
    const columnsLength = 5;
    const matrizNumbers = [...Array(rowsLength)].map(e => Array(columnsLength).fill('') );
    for (let i = 0; i < rowsLength; i++) {
      for (let j = 0; j < columnsLength; j++) {
        matrizNumbers[i][j] = Math.floor(Math.random() * (100 - (-100))) -100;   
      }
    }
    console.log('Matriz: ');
    console.log(matrizNumbers);
    const result = from2Dto1D(matrizNumbers, rowsLength, columnsLength, order);
    res.json(result);
});


/**
 * @swagger
 * /challenge/5_MyCows/{Cows}:
 *   get:
 *       summary: Calcula la producción diaria del hato, la vaca con mayor produccion por dia, la produccion mayor y la menor
 *       tags: [Challenges]
 *       parameters:
 *         - name: Cows
 *           in: path
 *           required: true
 *           description: Cantidad de vacas por dia
 *           schema:
 *               type: integer
 *       responses:
 *           200:
 *               description: Manipula una matriz y realiza operaciones
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/challenge5'
 */
function fillCowMatriz(cows){
  const matriz = [...Array(7)].map(e => Array(cows).fill('') );
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < cows; j++) {
      matriz[i][j] = Math.floor(Math.random() * (11.9 - (0))) - 0;   
    }
  }
  return matriz;
}
function weekHatoProduction(matriz, cows){
  let resultProdHato = [];
  for (let i = 0; i < 7; i++) {
    let contProdHato = 0;
    for (let j = 0; j < cows; j++) {
      contProdHato += matriz[i][j];
      if(j === cows-1)resultProdHato.push(contProdHato);      
    }
  }
  return resultProdHato; 
}
function maxProductionDay(production){
  let result = [];
  const maxProduction = Math.max(...production);
  const days = production.map( (value, i) => { if(value === maxProduction) return 'Day '+(i+1) });
  days.forEach( day => { if(day) result.push(day)});
  return result;
}
function minProductionDay(production){
  let result = [];
  const minProduction = Math.min(...production);
  const days = production.map( (value, i) => { if(value === minProduction) return 'Day '+(i+1) });
  days.forEach( day => { if(day) result.push(day)});
  return result;
}
function maxCows(matriz, cows){
  const older = [];
  const maxCow = [];
  for (let i = 0; i < 7; i++) {
    let aux = 0;
    for (let j = 0; j < cows; j++) {
      if(matriz[i][j] > aux) aux = matriz[i][j];
      if(j === cows - 1) older.push(aux);
    }
  }
  for (let i = 0; i < 7; i++) {
    let cowPerDay = '';
    for (let j = 0; j < cows; j++) {
      if(older[i] === matriz[i][j]){
        cowPerDay += ' Cow '+(j+1)+', ';
      }
      if(j === cows -1) maxCow.push('Day '+(i+1)+': '+cowPerDay);
    }
  }
  return maxCow;
}
router.get('/5_MyCows/:Cows', (req, res) => {
  const { Cows: N } = req.params;
  if(N >= 3 && N <= 50){
    const matrizNumbers = fillCowMatriz(N); 
    const ProdHato = weekHatoProduction(matrizNumbers, N);
    const resultProdHato = ProdHato.map( (value, i) => {return 'Day '+(i+1)+': '+value });  
    const resultMaxProductionDays = maxProductionDay(ProdHato);
    const resultMinProductionDays = minProductionDay(ProdHato);
    const resultMaxCows = maxCows(matrizNumbers, N);

    const result = {
      DailyHatoProduction: resultProdHato,
      MaxProductionDays: resultMaxProductionDays,
      MinProductionDays: resultMinProductionDays,
      DailyMaxCows: resultMaxCows,
    }
    console.log(matrizNumbers);
    res.json(result);

  }else res.json('Error: minimun cows allowed: 3 and maximum cows allowed: 50');

});

/**
 * @swagger
 * /challenge/6_TrackingCoordinadora/{codigo_remision}:
 *   get:
 *       summary: Consume Apis segun el codigo enviado por parametros
 *       tags: [Challenges]
 *       parameters:
 *         - name: codigo_remision
 *           in: path
 *           required: true
 *           description: Codigo segun el cual se envia una respuesta
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Da una respuesta segun un codigo enviado
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/challenge6'
 */
router.get('/6_TrackingCoordinadora/:codigo_remision', async (req, res) => {
  const { codigo_remision } = req.params;
  if(codigo_remision === 'GUIA' || codigo_remision === 'ETIQUETA'){
    try {
      if(codigo_remision === 'ETIQUETA'){
        const answer = await fetch('https://api.coordinadora.com/cm-model-testing/api/v1/talentos/');
        const result = await answer.json();
        res.json(result);
      } else {
        const answer = await fetch('https://api.coordinadora.com/cm-model-testing/api/v1/talentos/checkpoint');
        const result = await answer.json();
        res.json(result);
      }
    } catch (error) {
      console.error(error);
    }
  } else res.status(400).json('Error: Codigo de remision equivocado, solo se admite los codigos ETIQUETA y GUIA');
});

/**
 * @swagger
 * /challenge/7_ArrayScore/{N}:
 *   get:
 *       summary: Calcula los puntos de un array según sus numeros
 *       tags: [Challenges]
 *       parameters:
 *         - name: N
 *           in: path
 *           required: true
 *           description: Cantidad de numeros en el array
 *           schema:
 *               type: integer
 *       responses:
 *           200:
 *               description: Manipula un array y realiza operaciones
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/challenge7'
 */
function pointsCalculation(matriz){
  let pointsForEvenNumbers = 0;
  let pointsForOddNumbers = 0;
  let pointsForNumberFive = 0;

  matriz.forEach((element) => {
    if (element%2 === 0) pointsForEvenNumbers += 1;
    else{
      if(element === 5) pointsForNumberFive += 5;
      else pointsForOddNumbers += 3;
    } 
  }); 

  const points = pointsForEvenNumbers + pointsForOddNumbers + pointsForNumberFive;
  return points;
}
router.get('/7_ArrayScore/:N', (req, res) => {
  const { N } = req.params;
  const matriz = [];
  for (let i = 0; i < N; i++) { 
    matriz[i] = Math.floor(Math.random() * (100 - (0))) - 0; 
  }
  const points = pointsCalculation(matriz);
  res.json(`The  points of this array: [${matriz}] are: ${points}`);
});

/**
 * @swagger
 * tags:
 *  name: Challenges
 *  description: Sección de retos 
 * components:
 *  schemas:
 *      challenge1:
 *          type: object
 *          required:
 *              -a
 *              -b
 *          properties:
 *              a:
 *                  type: integer
 *                  description: Numero sumado a la operación
 *              b:
 *                  type: integer
 *                  description: Número dividido entre 2
 *          example:
 *              "El resultado es: 55"
 *      challenge2:
 *          type: object
 *          required:
 *              -word
 *          properties:
 *              word:
 *                  type: string
 *                  description: Cadena de texto enviada
 *          example:
 *              La palabra: "Hello!! tenia 2 simbolo(s), el resultado es: Hello!" 
 *      challenge3:
 *          type: object
 *          required:
 *              -rowsLength
 *              -columnsLength
 *          properties:
 *              rowsLength:
 *                  type: integer
 *                  description: Numero de filas
 *              columnsLength:
 *                  type: integer
 *                  description: Número de columnas
 *          example:
 *              sumMatriz: 340
 *              sumPositiveMatriz: 324
 *              sumEvenNumbers: 48
 *              sumOddNumbers: 139   
 *      challenge4:
 *          type: object
 *          required:
 *              -order
 *          properties:
 *              order:
 *                  type: string
 *                  description: Cadena de texto que define el orden ascendete o descendente de los numeros
 *          example:
 *              [56, 55, 34, 21, 0, -1, -34, -71]
 *      challenge5:
 *          type: object
 *          required:
 *              -Cows
 *          properties:
 *              Cows:
 *                  type: integer
 *                  description: Numero que define la cantidad de vacas en la hacienda
 *          example:
 *              DailyHatoProduction: ["Day 1: 32", "Day 2: 45", "Day 3: 5", "Day 4: 23", "Day 5: 45", "Day 6: 30", "Day 7: 54"]
 *              MaxProductionDays: ["Day 2: 54"]
 *              MinProductionDays: ["Day 3: 5"]
 *              DailyMaxCows: ["Day 1: Cow 1, Cow 2", "Day 2: Cow 3, Cow 5, Cow 6", "Day 3: Cow 2",  "Day 4: Cow 6", "Day 5: Cow 1", "Day 6: Cow 3", "Day 7: Cow 6"]
 *      challenge6:
 *          type: object
 *          required:
 *              -codigo_remision
 *          properties:
 *              word:
 *                  type: string
 *                  description: Codigo GUIA o ETIQUETA
 *          example:
 *              isError: false
 *              status: success
 *              data: [codigo_remision: "34380016861", nombre_destinatario: "LEIDYJULIETH RIVERA", dir_destinatario: "KR 8 # 123 - 154 TO 5 AP 304 CONJUNTO RESIDENCIAL TREVI"]    
 *      challenge7:
 *          type: object
 *          required:
 *              -N
 *          properties:
 *              N:
 *                  type: integer
 *                  description: Cantidad de numeros en el array
 *          example:
 *              "The  points of this array: [1, 2, 3, 4, 5] are: ${13}"
 */

export default router;